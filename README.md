# HOW_TO_USE_GIT

1) On your personal GITHUB account [https://github.com/DJRumble] create a new repository
2) Clone that repositotry to the desired folder from the cmd line `git clone https://github.com/DJRumble/HOW_TO_USE_GIT.git`
3) move to the directory `cd HOW_TO_USE_GIT/`
4) create a read me file to explain what is going on in that repo `echo "# HOW_TO_USE_GIT" >> README.md`
5) manually drag and drop any other content into that directory
6) Initialise the git repo `git init`
7) Add your content `git add README.m`
8) Push your content into the local stagging area `git commit -m "first commit"`
9) Connect your remote repo to the stagging area `git remote add origin https://github.com/DJRumble/HOW_TO_USE_GIT.git`
10) Push content `git push -u origin master`

At any point use `git status` to check the status of the repo you are in. 

A) If you have created a folder locally first, and then want to move this onto GIT. First create the repo on git.
B) On the local terminal, follow the following comands to initialise, add, commit work to the new repo. Use the Remote function to deffine the source location as the newly created repo. Then push.

`git init`

`git add .`

`git commit -m "Push existing project to GitLab" `

`git remote add source https://gitlab.com/cameronmcnz/example-website.git `

`git push -u -f source master `

## Git ignore

Git ignore is useful for excluding certain files, for example very large ones of tmp files, from being pushed to git. This keeps the repo clean and easy to use, as well as derisking large files that may prevent it from being used. 

1) Create a .gitignore file `touch .gitignore` 
2) Edit the file with a local text editior, for example `vi .gitignore`
3) Include the folder, or extension type, to be ignored, for example `*.log` or `data/`
4) push to gitlab
5) make further edits in termianl or in gitlab

## Reverse add

If you have added something to git that you don't want you can remove them individually with...

`git rm [file]`

or if you want to reset everything you can use

`git reset HEAD -- .`

## Branches

If you want to see what branches are available use

`git branch -a`

Create a branch with 

`git branch <test_branch_name>`

change branches with 

`git checkout <test_branch_name>`